# These commands are used for dev work, and are not needed for general usage, install chord-finder with pip

.PHONY: clean build install all

all: uninstall clean build install

clean:
	rm build -rf

build: clean
	python3 setup.py build

install:
	sudo python3 setup.py install

uninstall:
	sudo rm -rf /etc/chord-finder /usr/lib/python*/site-packages/chordfinder /usr/bin/chord-finder.py

upload:
	./setup.py sdist
	twine upload dist/*

