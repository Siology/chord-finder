""" File that contains the gui related code """

import wx
import tempfile
from typing import Optional
from .DrawChord import *
from .DrawRootFinder import *
from .debugging import *


class FilterError(Exception):
	""" Place-holder class for custom error raising """
	pass


class MainFrame(wx.Frame):
	""" The gui class  """
	# Begin event handler functions

	#@dump_args
	def on_about(self, event) -> None:
			"""Triggered when "about" is selected from menus, displays a simple dialog window"""
			message = 'Welcome to chord finder\nPlease report bugs to:\nsiology.io@gmail.com'
			dialog = wx.MessageDialog(self, message, 'About chord finder', wx.OK | wx.ICON_INFORMATION)
			dialog.Show()
			dialog.Destroy()

	@dump_args
	def on_chord_changed(self, chord_db: ChordDatabase, config: {}) -> None:
			"""Triggered when chord name dropdown list is altered. Sets up images for new chord"""
			chord_name = self.chord_name_combo.GetValue()

			chord_output_file = tempfile.NamedTemporaryFile('wb')
			movable_image = tempfile.NamedTemporaryFile('wb')

			instrument = Instrument()
			palette = Palette(self.palette_radio.GetSelection())

			try:
					chord_obj = chord_db.get_single_chord_object(chord_type=self.chord_type_radio.GetStringSelection(),
																																																	chord_name=chord_name)
			except ChordNotFound:
					self.SetStatusText(f"Chord: {chord_name} was not found in chord data archive")
			else:
					if self.chord_type_radio.GetStringSelection() == 'Movable':
							draw_root_finder(instrument=instrument, palette=palette, chord=chord_obj, image_dimensions=(290, 100),
																							 output_file=movable_image.name)
							self.movable_bitmap.SetBitmap(wx.Bitmap(movable_image.name, wx.BITMAP_TYPE_ANY))
							self.movable_bitmap.Show()
					else:
							self.movable_bitmap.Hide()
							self.Refresh()

					draw_chord(instrument=instrument, palette=palette, chord_type=chord_obj.chord_type,
															 finger_data_str=chord_obj.finger_str, fret_data_str=chord_obj.fret_str,
															 image_dimensions=(290, 490), output_file=chord_output_file, config=config)
					self.chord_bitmap.SetBitmap(wx.Bitmap(chord_output_file.name))
					self.SetStatusText(f"Showing: {chord_obj.long_name}")


	@dump_args
	def prev_or_next_chord(self, direction: int, chord_db: ChordDatabase, config: {}) -> None:
			"""Shift selected chord by one position"""
			curr_selected = self.chord_name_combo.GetSelection()
			# Shift to previous chord if direction is -1 and not at the first item
			self.chord_name_combo.SetSelection(curr_selected - 1) if direction == -1 and curr_selected != 0 else None
			# Shift to next chord, auto-cycling to first item if needed
			self.chord_name_combo.SetSelection(curr_selected + 1) if direction == 1 else None
			# Move to the last item in the list
			self.chord_name_combo.SetSelection(self.chord_name_combo.GetCount() - 1) if direction != -1 and direction != 1 else None
			self.on_chord_changed(self, chord_db=chord_db, config=config)


	@dump_args
	def on_filter_changed(self, chord_db: ChordDatabase, config: {}) -> None:
		""" Triggered when filter radio box is altered
		Filter the available chords to allow for the updated filter settings"""

		# Clear the chord name combo box
		self.chord_name_combo.Clear()

		# Map the selected augmentation/diminished option to the corresponding long name
		aug_dim_wanted = self.aug_dim_combo.GetStringSelection()
		aug_dim_wanted_long = {
			'All': 'All',
			'aug': 'Augmented',
			'dim': 'Diminished',
			'dom': 'Dominant',
			'm': 'Minor',
			'M': 'Major',
			'sus': 'Suspended'
		}.get(aug_dim_wanted)

		# Raise an error if the selected augmentation/diminished option is invalid
		if aug_dim_wanted_long is None:
			raise FilterError

		# Get the filtered chord names based on the selected filter settings and append them to the chord name combo box
		self.chord_name_combo.AppendItems(
			chord_db.get_filtered_chord_names(
				self.chord_type_radio.GetStringSelection(),
				self.root_filter_combo.GetStringSelection(),
				aug_dim_wanted_long
			)
		)

		# Set the combo box value to the first entry and trigger the on_chord_changed method
		self.chord_name_combo.SetValue(self.chord_name_combo.GetString(0))
		self.on_chord_changed(chord_db=chord_db, config=config)


	@dump_args
	def on_exit(self, event) -> None:
			"""Exit function."""
			self.Close(True)

	@dump_args
	def on_play_chord(self, chord_db: ChordDatabase) -> Optional[int]:
			chord_name = self.chord_name_combo.GetValue()

			if not chord_name:
					return 1

			try:
					chord_type = self.chord_type_radio.GetStringSelection()
					chord_obj = chord_db.get_single_chord_object(chord_type=chord_type, chord_name=chord_name)
			except ChordNotFound:
					self.SetStatusText(f"Chord: {chord_name} was not found in chord data archive")
			else:
					chord_db.play_chord_from_obj(chord_obj=chord_obj)
# END OF EVENT HANDLERS

	@dump_args
	def __init__(self, parent: int, ident: int, title: str, chord_db: ChordDatabase, config: {}) -> None:
		"""Main gui class"""
		wx.Frame.__init__(self, parent, ident, title, wx.DefaultPosition, wx.Size(500, 600))

		favicon = wx.Icon(config['data_dir']+'favicon.ico', wx.BITMAP_TYPE_ICO, 16, 16)
		wx.Frame.SetIcon(self, favicon)
		self.menu_bar = wx.MenuBar()

		# Menu bar
		id_about = 101
		id_exit = 102
		self.CreateStatusBar()
		self.SetStatusText('Welcome to Chord Finder')
		menu = wx.Menu()
		about_menu_item = menu.Append(id_about, '&About', 'More information about this program')
		menu.AppendSeparator()
		exit_menu_item = menu.Append(id_exit, 'E&xit', 'Terminate the program')
		menu_bar = wx.MenuBar()
		menu_bar.Append(menu, '&File')
		self.SetMenuBar(menu_bar)
		self.Bind(wx.EVT_MENU, self.on_about, about_menu_item)
		self.Bind(wx.EVT_MENU, self.on_exit, exit_menu_item)
		# End of Menu bar

		# The bitmap areas
		self.welcome_label = wx.StaticText(self, -1, 'Welcome to chord finder')
		placeholder = wx.Bitmap(290,490)
		self.chord_bitmap = wx.StaticBitmap(self, bitmap=placeholder)
		self.movable_bitmap = wx.StaticBitmap(self, -1)

		# Static / Movable chord type combo box and its label
		self.chord_type_radio = wx.RadioBox(self, -1, 'Type', choices=['Static', 'Movable'], majorDimension=1, style=wx.RA_SPECIFY_ROWS)
		self.chord_type_radio.Bind(wx.EVT_RADIOBOX, lambda evt, cdb=chord_db, cfg=config: self.on_filter_changed(cdb, cfg))

		# Dropdown list of chord names with prev/next buttons
		self.prev_chord_button = wx.Button(self, -1, label='<', size=(30, 30))
		self.chord_name_combo = wx.ComboBox(self, -1, choices=chord_db.get_all_chord_names(), style=wx.CB_DROPDOWN, size=(130, 30))
		self.next_chord_button = wx.Button(self, -1, label='>', size=(30, 30))

		self.chord_name_combo.Bind(wx.EVT_COMBOBOX, lambda evt, cdb=chord_db, cfg=config: self.on_chord_changed(chord_db=cdb, config=cfg))
		self.prev_chord_button.Bind(wx.EVT_BUTTON, lambda evt, cdb=chord_db, cfg=config, d=-1: self.prev_or_next_chord(direction=d, chord_db=cdb, config=cfg))
		self.next_chord_button.Bind(wx.EVT_BUTTON, lambda evt, cdb=chord_db, cfg=config, d=1: self.prev_or_next_chord(direction=d, chord_db=cdb, config=cfg))

		# Play chord button
		self.play_chord_button = wx.Button(self, -1, label='Play chord', size=(100, 30))
		self.play_chord_button.Bind(wx.EVT_BUTTON, lambda evt, cdb=chord_db: self.on_play_chord(chord_db=cdb))

		# Palette choice radio box
		self.palette_radio = wx.RadioBox(self, -1, 'Palette', choices=['Colour', 'B/W'], majorDimension=1, style=wx.RA_SPECIFY_ROWS)
		self.palette_radio.Bind(wx.EVT_RADIOBOX, lambda evt, cdb=chord_db, cfg=config: self.on_filter_changed(chord_db=cdb, config=cfg))

		# Root filter combobox and its label
		self.root_filter_label = wx.StaticText(self, -1, 'Root:')
		self.root_filter_combo = wx.ComboBox(self, -1, choices=['All', 'A', 'B', 'C', 'D', 'E', 'F', 'G'], style=wx.CB_DROPDOWN, size=(130, 30))
		self.root_filter_combo.Bind(wx.EVT_COMBOBOX, lambda evt, cdb=chord_db, cfg=config: self.on_filter_changed(chord_db=cdb, config=cfg))

		# Aug / Dom combo box and its label
		self.aug_dim_label = wx.StaticText(self, -1, 'Chord mode:')
		self.aug_dim_combo = wx.ComboBox(self, -1, choices=['All', 'M', 'm', 'dom', 'dim', 'aug', 'sus'], style=wx.CB_DROPDOWN, size=(130, 30))
		self.aug_dim_combo.Bind(wx.EVT_COMBOBOX, lambda evt, cdb=chord_db, cfg=config: self.on_filter_changed(chord_db=cdb, config=cfg))

		# Set this all up
		self.set_properties(config=config)
		self.do_layout()

	@dump_args
	def set_properties(self, config: {}) -> None:
		"""Deal with any property setting needs"""
		self.SetTitle('Guitar Chord Finder: ' + config['version'])
		self.chord_bitmap.SetMinSize((290, 490))
		self.movable_bitmap.SetMinSize((290, 100))
		self.chord_type_radio.SetSelection(0)
		self.root_filter_combo.SetSelection(0)
		self.aug_dim_combo.SetSelection(0)
		self.chord_name_combo.SetSelection(-1)


	@dump_args
	def do_layout(self) -> None:
			"""Deal with the arrangement of the widgets"""

			# Create the sizers
			main_area_sizer = wx.BoxSizer(wx.HORIZONTAL)
			right_half_sizer = wx.BoxSizer(wx.VERTICAL)
			chord_select_sizer = wx.BoxSizer(wx.HORIZONTAL)
			root_filter_sizer = wx.BoxSizer(wx.HORIZONTAL)
			aug_dim_sizer = wx.BoxSizer(wx.HORIZONTAL)

			# Fill the sizers and create hierarchy
			main_area_sizer.Add(self.chord_bitmap, flag=wx.EXPAND)

			chord_select_sizer.Add(self.prev_chord_button, flag=wx.EXPAND)
			chord_select_sizer.Add(self.chord_name_combo, flag=wx.EXPAND)
			chord_select_sizer.Add(self.next_chord_button, flag=wx.EXPAND)

			root_filter_sizer.Add(self.root_filter_label)
			root_filter_sizer.Add(self.root_filter_combo)

			aug_dim_sizer.Add(self.aug_dim_label)
			aug_dim_sizer.Add(self.aug_dim_combo)

			# Create the sizer hierarchy
			filters_box = wx.StaticBox(self, wx.VERTICAL, label='Filters')

			filters_sizer = wx.StaticBoxSizer(filters_box, wx.VERTICAL)
			filters_sizer.Add(self.chord_type_radio, flag=wx.ALIGN_RIGHT)
			filters_sizer.Add(root_filter_sizer, flag=wx.ALIGN_RIGHT)
			filters_sizer.Add(aug_dim_sizer)

			right_half_sizer.Add(self.palette_radio)
			right_half_sizer.Add(chord_select_sizer)
			right_half_sizer.Add(self.play_chord_button)
			right_half_sizer.Add(filters_sizer)
			right_half_sizer.Add(self.movable_bitmap)

			main_area_sizer.Add(right_half_sizer)

			# Display the result
			self.SetSizer(main_area_sizer)
			main_area_sizer.Fit(self)
			self.Layout()
