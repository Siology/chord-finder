# Borrowed from https://stackoverflow.com/questions/6200270/decorator-that-prints-function-call-details-parameters-names-and-effective-valu

import logging
import inspect

def dump_args(func):
    """Decorator to print function call details.
    This includes parameter names and effective values.

    Args:
        func: The function to be decorated.

    Returns:
        The decorated function.
    """
    def wrapper(*args, **kwargs):
        # Get the arguments passed to the function
        func_args = inspect.signature(func).bind(*args, **kwargs).arguments
        # Convert the arguments to a string representation
        func_args_str = ", ".join(map("{0[0]}={0[1]!r}".format, func_args.items()))
        # Log the function call details
        logging.debug(f"{func.__module__}.{func.__qualname__} ( {func_args_str} )")
        # Call the original function
        return func(*args, **kwargs)
    return wrapper