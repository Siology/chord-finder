""" Module for drawing the main chord image """

import PIL.Image, PIL.ImageDraw, PIL.ImageFont, os
from .Instruments import *
from .Palettes import *
from typing import List, Tuple

@dump_args
def draw_frets(draw: PIL.ImageDraw, instrument: Instrument, movable_shift: int, image_width: int, palette: Palette,
							 font_path: str, config: {}) -> PIL.ImageDraw:
		"""Add the frets onto the image"""
		for fret_num in range(1, instrument.number_of_frets - movable_shift):
				fret_pos_y = fret_num * instrument.fret_spacing + instrument.top_gap + instrument.nut_width
				draw.line((instrument.margin, fret_pos_y, image_width, fret_pos_y), palette.fret_colour, width=3)

				if fret_num % 2 != 0:  # only draw odd numbered frets (less screen clutter)
						# Calculate coords to draw a triangular fret side markings
						point_a = (instrument.margin - 10, fret_num * instrument.fret_spacing + instrument.top_gap + instrument.string_width - 5)
						point_b = (instrument.margin - 3, fret_num * instrument.fret_spacing + instrument.top_gap + instrument.string_width)
						point_c = (instrument.margin - 10, fret_num * instrument.fret_spacing + instrument.top_gap + instrument.string_width + 5)

						draw.polygon((point_a, point_b, point_c), palette.fret_tri_colour)
						sans16 = PIL.ImageFont.truetype(font_path, 20)
						text_pos = (3, fret_num * instrument.fret_spacing + instrument.top_gap - 10)
						draw.text(text_pos, 'F' + str(fret_num + movable_shift), font=sans16, fill=palette.tri_text_colour)
		return draw


@dump_args
def draw_fret_markers(draw: PIL.ImageDraw, instrument: Instrument, movable_shift: int, palette: Palette) -> PIL.ImageDraw:
		"""Add the fret markers onto the image"""

		for fret_pos in instrument.marker_positions:
				y_pos = (instrument.top_gap + instrument.nut_width) + ((fret_pos - movable_shift) * instrument.fret_spacing) - (instrument.fret_spacing / 2)

				if y_pos <= instrument.top_gap:
						continue

				if fret_pos in instrument.double_markings:
						x_pos_1 = instrument.margin + instrument.first_string_gap + instrument.string_spacing
						x_pos_2 = instrument.margin + instrument.first_string_gap + instrument.string_spacing * 4
						draw.ellipse((x_pos_1 - instrument.marker_size, y_pos - instrument.marker_size,
													x_pos_1 + instrument.marker_size, y_pos + instrument.marker_size),
												 palette.fret_marker_colour)
						draw.ellipse((x_pos_2 - instrument.marker_size, y_pos - instrument.marker_size,
													x_pos_2 + instrument.marker_size, y_pos + instrument.marker_size),
												 palette.fret_marker_colour)
				else:
						x_pos = instrument.margin + instrument.first_string_gap + ((instrument.number_of_strings / 2) * instrument.string_spacing) - (instrument.string_spacing / 2)
						draw.ellipse((x_pos - instrument.marker_size, y_pos - instrument.marker_size,
													x_pos + instrument.marker_size, y_pos + instrument.marker_size),
												 palette.fret_marker_colour)

		return draw


@dump_args
def draw_strings(draw: PIL.ImageDraw, instrument: Instrument, image_height: int, palette: Palette) -> PIL.ImageDraw:
		"""Add the strings onto the image"""
		for string_num in range(instrument.number_of_strings):
				string_pos_x = (string_num * instrument.string_spacing) + instrument.first_string_gap + instrument.margin
				string_pos_y = instrument.top_gap + instrument.nut_width
				draw.line((string_pos_x, string_pos_y, string_pos_x, image_height), palette.string_colour, width=instrument.string_width)
		return draw


@dump_args
def draw_barre_arc(draw: PIL.ImageDraw, instrument: Instrument, finger_data_list: [int],
									 string_num: int, x_pos: int, y_pos: int, palette) -> str:
		"""Add barre arcs to the image if needed"""
		finger_str = str(finger_data_list[string_num])
		if "-" in finger_str:
				arc_end = int(finger_str[3])
				x_pos_start = x_pos - 20
				y_pos_start = y_pos - 30
				x_pos_end = (x_pos + (instrument.string_spacing * ((arc_end - 1) - string_num))) + 20
				y_pos_end = y_pos + 60
				draw.arc((x_pos_start, y_pos_start, x_pos_end, y_pos_end), 225, 315, fill=palette.arc_colour)
		return finger_str


@dump_args
def draw_finger_circles(
		draw: PIL.ImageDraw,
		instrument: Instrument,
		movable_shift: int,
		finger_data_list: [str],
		fret_num: int,
		string_num: int,
		font_path: str,
		palette: Palette
) -> None:
		"""Draw finger circles"""
		x_pos = (
				string_num * instrument.string_spacing
				+ instrument.first_string_gap
				+ instrument.margin
		)
		y_pos = (
				instrument.top_gap
				+ instrument.nut_width
				+ instrument.fret_spacing * (fret_num - movable_shift)
				- instrument.fret_spacing / 2
		)

		draw.ellipse(
				(
						x_pos - instrument.finger_size,
						y_pos - instrument.finger_size,
						x_pos + instrument.finger_size,
						y_pos + instrument.finger_size,
				),
				palette.finger_circle_colour,
		)

		finger_str = draw_barre_arc(
				draw, instrument, finger_data_list, string_num, x_pos, y_pos, palette
		)

		x_pos -= instrument.finger_font_size / 2
		y_pos -= instrument.finger_font_size / 2
		sans16 = PIL.ImageFont.truetype(font_path, 20)
		draw.text(
				(x_pos + 4, y_pos),
				finger_str,
				font=sans16,
				fill=palette.finger_text_colour,
		)

@dump_args
def draw_string_data_and_finger_circles(draw: PIL.ImageDraw, instrument: Instrument, finger_data_list: [str],
																				movable_shift: int, fret_data_list: [str], font_path: str, palette: Palette) -> PIL.ImageDraw:
	"""Draw open / close string data ("X" or "O"), and fingering circles"""
	for string_num, fret_num in enumerate(fret_data_list):
		fret_num = int(fret_num)

		x_pos = (string_num * instrument.string_spacing) + instrument.first_string_gap + instrument.margin
		y_pos = instrument.top_gap / 2

		if fret_num == -1:  # An "X" is needed
			draw.line((x_pos - instrument.top_gap_data_size, y_pos - instrument.top_gap_data_size,
								 x_pos + instrument.top_gap_data_size, y_pos + instrument.top_gap_data_size), palette.xo_colour,
								width=2)
			draw.line((x_pos - instrument.top_gap_data_size, y_pos + instrument.top_gap_data_size,
								 x_pos + instrument.top_gap_data_size, y_pos - instrument.top_gap_data_size), palette.xo_colour,
								width=2)
		elif fret_num == 0:  # An "O" is needed
			draw.ellipse((x_pos - instrument.top_gap_data_size, y_pos - instrument.top_gap_data_size,
										x_pos + instrument.top_gap_data_size, y_pos + instrument.top_gap_data_size), outline=None)
		elif fret_num > 0:  # not -1 or 0 of "X" or "O", must be fingering information
			draw_finger_circles(draw, instrument, movable_shift, finger_data_list, fret_num, string_num, font_path,
													palette)

	return draw


@dump_args
def draw_chord(
		instrument: Instrument,
		palette: Palette,
		chord_type: str,
		finger_data_str: str,
		fret_data_str: str,
		image_dimensions: Tuple[int, int],
		output_file: str,
		config: List
) -> None:
		finger_data_list = finger_data_str.split()
		fret_data_list = fret_data_str.split()

		font_paths = [
				'/usr/share/fonts/truetype/dejavu/DejaVuSerifCondensed-Bold.ttf',
				'/Library/Fonts/Arial Unicode.ttf'
		]

		for font_path in font_paths:
				if os.path.exists(font_path):
						break
		else:
				raise FileNotFoundError

		movable_shift = 0
		if chord_type == 'Movable':
				min_fret = min(int(f) for f in fret_data_list if int(f) > 0)
				movable_shift = min_fret - 2

		image_width, image_height = image_dimensions
		image = PIL.Image.new('RGB', image_dimensions)

		draw = PIL.ImageDraw.Draw(image)
		draw.polygon(
				[
						(instrument.margin, instrument.top_gap + instrument.nut_width),
						(image_width, instrument.top_gap + instrument.nut_width),
						(image_width, image_height),
						(instrument.margin, image_height)
				],
				palette.fret_board_colour
		)

		if movable_shift == 0:
				draw.line(
						(instrument.margin, instrument.top_gap, image_width, instrument.top_gap),
						palette.nut_colour,
						width=8
				)

		draw = draw_frets(draw, instrument, movable_shift, image_width, palette, font_path, config)
		draw = draw_fret_markers(draw, instrument, movable_shift, palette)
		draw = draw_strings(draw, instrument, image_height, palette)
		draw_string_data_and_finger_circles(
				draw, instrument, finger_data_list, movable_shift, fret_data_list, font_path, palette
		)

		del draw
		image.save(output_file, 'PNG')