import os, sys
from typing import Optional
from .debugging import *


class ChordNotFound(Exception):
	pass


class Chord:
	"""A class that holds data for a single static chord"""
	@dump_args
	def __init__(self, chord_type: str, root: str, short_name: str, long_name: str, finger_str: str, fret_str: str) -> None:
		self.chord_type = chord_type
		self.root = root
		self.short_name = short_name
		self.long_name = long_name
		self.finger_str = finger_str
		self.fret_str = fret_str

class ChordDatabase:
	""" A class which builds up lists of all known chords and presents data on them as needed """
	@dump_args
	def __init__(self, chords_csv: str) -> None:

		self.chord_list = []
		self.init_chords(chords_csv)

	@dump_args
	def init_chords(self, csv_file: str) -> None:
			"""Build up an array of all the chord data from the chord data file"""
			try:
					with open(csv_file, 'r') as file_obj:
							for line in file_obj:
									if line.startswith('#') or not line.strip():  # ignore comment lines and empty lines
											continue
									chord_type, root, short_name, long_name, finger_str, fret_str = line.split(',')
									chord_obj = Chord(chord_type, root, short_name, long_name, finger_str, fret_str)
									self.chord_list.append(chord_obj)
			except IOError:
					print(f"IOError opening: {csv_file}")
					sys.exit(1)

	@dump_args
	def get_single_chord_object(self, chord_type: str, chord_name: str) -> Optional[Chord]:
			"""Return a single chord object from the list of chords"""
			for chord in self.chord_list:
					if chord.short_name == chord_name and chord.chord_type == chord_type:
							return chord
			raise ChordNotFound("Chord not found")

	@dump_args
	def get_all_chord_names(self) -> [str]:
			"""Return a list of all the chord names"""
			return [chord.short_name for chord in self.chord_list]

	@dump_args
	def get_filtered_chord_names(self, chord_type: str, root: str, dim_aug: str) -> [str]:
			"""Return a filtered list of chord names based on specified criteria"""
			return [chord.short_name for chord in self.chord_list
							if chord.chord_type == chord_type
							and (root == 'All' or chord.root == root)
							and (dim_aug == 'All' or dim_aug in chord.long_name)]

	@dump_args
	def play_chord_from_obj(self, chord_obj: Chord) -> int:
			open_strings = ['E2', 'A2', 'D3', 'G3', 'B3', 'E4']
			notes = ['A', 'A#', 'B', 'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#']
			fret_data = chord_obj.fret_str.split()
			chord_str = ''

			for i, fret_pos in enumerate(fret_data):
					if fret_pos == '0':
							chord_str += open_strings[i] + " "
							continue
					elif fret_pos == '-1':
							continue

					open_note = open_strings[i][0]
					open_octave = int(open_strings[i][1])

					base_pos = notes.index(open_note)
					shift_pos = base_pos + int(fret_pos)
					final_note = notes[shift_pos % len(notes)] if shift_pos >= len(notes) else notes[shift_pos]
					octave_shift = int(shift_pos / len(notes))
					final_octave = open_octave + octave_shift
					output_note = final_note + str(final_octave)
					chord_str += output_note + " "

			cmd_str = f"for p in {chord_str}; do ( play -n synth 3 pluck $p vol 0.1 >/dev/null 2>&1 &); done"
			rc = os.system(cmd_str)
			return rc