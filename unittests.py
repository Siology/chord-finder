import unittest
from chordfinder import ChordData


class UnitTests(unittest.TestCase):
		config = {}

		@classmethod
		def setUpClass(self):
			self.config = {'data_dir': 'data/'}
			self.class_chorddata = ChordData.ChordDatabase(self.config['data_dir']+'ChordData.csv')

		def test_chorddata(self):
			chord_data = ChordData.ChordDatabase(self.config['data_dir']+'ChordData.csv')
			return True

		def test_count_chords(self):
			self.assertEqual(len(self.class_chorddata.get_all_chord_names()), 115)

		def test_get_filtered_chord_names_1(self):
			chords = self.class_chorddata.get_filtered_chord_names(chord_type='Static', root='A', dim_aug='All')
			self.assertEqual(len(chords), 13)

		def test_get_filtered_chord_names_2(self):
			chords = self.class_chorddata.get_filtered_chord_names(chord_type='Static', root='E', dim_aug='Dominant')
			self.assertEqual(len(chords), 3)

		def test_get_filtered_chord_names_3(self):
			chords = self.class_chorddata.get_filtered_chord_names(chord_type='Movable', root='C', dim_aug='Major')
			self.assertEqual(len(chords), 1)

		def test_get_filtered_chord_names_3(self):
			chords = self.class_chorddata.get_filtered_chord_names(chord_type='Movable', root='All', dim_aug='All')
			self.assertEqual(len(chords), 51)

		def test_play_chord_from_obj(self):
			chord_obj = self.class_chorddata.get_single_chord_object(chord_type="Static", chord_name="C")
			rc = self.class_chorddata.play_chord_from_obj(chord_obj=chord_obj)
			self.assertEqual(rc, 0)

if __name__ == '__main__':
		unittest.main()